﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using CommonSourcesProject;

namespace SceneParser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private OperatingController operatingController = new OperatingController();
        private OperatingModel operatingModel = new OperatingModel();

        private ViewModel view = new ViewModel();
        private WorkFlowHelper workFlowHelper = new WorkFlowHelper();

        private ProcessingController processingController= new ProcessingController();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void SelectProjectFolderTxt_Copy_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(operatingController.CheckPath(SelectProjectFolderTxt.Text)){

                operatingModel.ProjectPath = SelectProjectFolderTxt.Text;
                view.ProjectFolderIsValid = true;
                SetScenesList();
            }
            else
            {
                view.ProjectFolderIsValid = false;
            }
        }

        private void SetScenesList()
        {
            operatingModel.Scenes = GetScenesList();
            ScenesListComboBox.ItemsSource = operatingModel.Scenes;
        }

        private List<string> GetScenesList()
        {
            return operatingController.FindFilesOnUnityProject(operatingModel.ProjectPath, "*.unity");
        }

        private void SelectDestinationFolderTxt_Copy_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (operatingController.CheckPath(SelectDestinationFolderTxt.Text))
            {

                operatingModel.DestinationPath = SelectDestinationFolderTxt.Text;
                view.DestinationFolderIsValid = true;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (view.SceneIsSelected && view.DestinationFolderIsValid)
            {
                workFlowHelper.ReadUnityFile(operatingModel.SelectedScene);
                workFlowHelper.ConvertYamlStringToObject();
                workFlowHelper.CovertSceneObjectToJsonString();
                workFlowHelper.SaveSceneJsonFile(operatingModel.DestinationPath+OperatingConsts.SCENE_MODEL_FILE);
                workFlowHelper.CreateExternalFilesList();
                workFlowHelper.SetFilePathToExternalFileObjects(operatingModel.ProjectPath);
                workFlowHelper.CopyResourceFilesToModel(operatingModel.ProjectPath, operatingModel.DestinationPath + OperatingConsts.SCENE_MODEL_RESOURCES_PATH);
                workFlowHelper.CovertExternalFileObjectsToJsonString();
                workFlowHelper.SaveExternalFilesJsonFile(operatingModel.DestinationPath + OperatingConsts.MODEL_EXRERNAL_FILES_FILE);
            }
        }

        private void SelectDestinationFolderBtn_Click(object sender, RoutedEventArgs e)
        {
            
            SelectDestinationFolderTxt.Text = operatingController.GetDirectory();
        }

        private void SelectProjectFolderBtn_Click(object sender, RoutedEventArgs e)
        {
            SelectProjectFolderTxt.Text = operatingController.GetDirectory();
        }

        private void ScenesListComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(operatingController.CheckSceneFileExist(ScenesListComboBox.SelectedItem as string))
            {
                operatingModel.SelectedScene = ScenesListComboBox.SelectedItem as string;
                view.SceneIsSelected = true;
            }
        }
    }
}
