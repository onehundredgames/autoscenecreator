﻿using System.Collections.Generic;

namespace SceneParser
{
    public class OperatingModel
    {
        private string projectPath;
        private string destinationPath;
        private List<string> scenes;
        private string selectedScene;


        public OperatingModel()
        {
        }

        public string ProjectPath
        {
            set { projectPath = value; }
            get { return projectPath; }
        }

        public string DestinationPath
        {
            set { destinationPath = value; }
            get { return destinationPath; }
        }

        public List<string> Scenes
        {
            set { scenes = value; }
            get { return scenes; }
        }

        public string SelectedScene
        {
            set { selectedScene = value; }
            get { return selectedScene; }
        }

    }
}
