﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using CommonSourcesProject;

namespace SceneParser
{
    class ProcessingController
    {
        private ProcessingModel processingModel = new ProcessingModel();

        public void Parse(List<string> sceneStrings)
        {
            processingModel.sourceSceneObjectsList = new List<SceneObject>();
            RemoveServiceInformation(sceneStrings);
            while (ExtractSceneObject(sceneStrings, processingModel.sourceSceneObjectsList)) { };
        }

        private bool ExtractSceneObject(List<string> sceneStrings, List<SceneObject> sceneObjects)
        {
            if(sceneStrings.Count== 0)
            {
                return false;
            }
            StringBuilder objectsString = new StringBuilder();
            string objectHeader = sceneStrings[0];
            int countUsesStrings = 1;
            for (int i = 1; i<sceneStrings.Count; i++)
            {
                
                string objectPropertie = sceneStrings[i];
                if (IsObjectHeader(objectPropertie))
                {
                    break;
                }
                else
                {
                    objectsString.AppendLine(objectPropertie);
                    countUsesStrings++;
                }
            }

            sceneStrings.RemoveRange(0, countUsesStrings);

            SceneObject sceneObject = new SceneObject()
            {
                ObjectHeader = objectHeader,
                ObjectBodyYaml = objectsString.ToString()
            };
            sceneObjects.Add(sceneObject);
            return true;
       }

        private bool IsObjectHeader(string objectPropertie)
        {
            string key = ParserConst.UNITY_SCENE_OBJECT_KEY;
            return objectPropertie.Contains(key);
        }

        private void RemoveServiceInformation(List<string> sceneStrings)
        {
            sceneStrings.RemoveRange(0,2);
        }

        public string GetJson()
        {
            string json = JsonConvert.SerializeObject(processingModel.sourceSceneObjectsList);
            return json;
        }

    }
}
