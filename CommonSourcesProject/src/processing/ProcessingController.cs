﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonSourcesProject
{ 
    public class ProcessingController
    {
        public List<SceneObject> ConvertUnitySceneStringToObjects(string sourceString)
        {
            List<SceneObject> sceneObjects = new List<SceneObject>();
            string[] sourceStrings = sourceString.Split(new string[] { "\n" }, StringSplitOptions.None);
            List<string> sourceStringsList = sourceStrings.ToList<string>();

            RemoveServiceInformation(sourceStringsList);
            while (ExtractSceneObject(sourceStringsList, sceneObjects)) { };

            return sceneObjects;
        }

        public string GetJsonFromObjects<T>(List<T> objects)
        {
            return JsonConvert.SerializeObject(objects);
        }

        public string ConvertUnitySceneObjectsToString(List<SceneObject> sceneObjects)
        {
            List<string> sceneStrings = new List<string>();
            AddHeader(sceneStrings);
            for (int i = 0; i < sceneObjects.Count; i++)
            {
                SceneObject sceneObject = sceneObjects[i];
                sceneStrings.Add(sceneObject.getObjectHeader());
                sceneStrings.Add(sceneObject.getObjectBodyYaml());
            }

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < sceneStrings.Count; i++)
            {
                stringBuilder.AppendLine(sceneStrings[i]);
            }
            return stringBuilder.ToString();
        }

        public List<SceneObject> GetSceneObjectsFromJson(string jsonSceneObjectsList)
        {
            JArray sceneModel = JsonConvert.DeserializeObject(jsonSceneObjectsList) as JArray;
            List<SceneObject> sceneObjects = new List<SceneObject>();

            for (int i = 0; i < sceneModel.Count; i++)
            {
                JObject sceneModelObject = sceneModel[i] as JObject;
                SceneObject sceneObject = new SceneObject()
                {
                    ObjectSceneId = sceneModelObject.GetValue("ObjectSceneId").ToString(),
                    ObjectTypeId = sceneModelObject.GetValue("ObjectTypeId").ToString()

                };

                var expConverter = new ExpandoObjectConverter();
                sceneObject.ObjectBody = JsonConvert.DeserializeObject<ExpandoObject>(sceneModelObject.GetValue("ObjectBody").ToString(), expConverter);

                sceneObjects.Add(sceneObject);
            }

            return sceneObjects;
        }

        public List<ExternalFileObject> FindExternalFilesInSceneObjects(List<SceneObject> sceneObjects)
        {
            List<ExternalFileObject> externalFileObjects = new List<ExternalFileObject>();
            List<string> uniqueFilesIdsList = new List<string>();
            for (int i = 0; i < sceneObjects.Count; i++)
            {
                List<string> filesIdsList = sceneObjects[i].GetUsesFilesInObject();
                foreach (string fileId in filesIdsList)
                {
                    if (uniqueFilesIdsList.FindIndex(fileId.Equals) == -1)
                    {
                        uniqueFilesIdsList.Add(fileId);
                    }
                }
            }

            for (int i = 0; i < sceneObjects.Count; i++)
            {
                int fileIndex = uniqueFilesIdsList.FindIndex(sceneObjects[i].ObjectSceneId.Equals);
                if (fileIndex != -1)
                {
                    uniqueFilesIdsList.RemoveAt(fileIndex);
                }
            }

            foreach(string fileId in uniqueFilesIdsList)
            {
                externalFileObjects.Add(new ExternalFileObject(fileId));
            }

            return externalFileObjects;
        }

        private bool ExtractSceneObject(List<string> sceneStrings, List<SceneObject> sceneObjects)
        {
            if (sceneStrings.Count == 0)
            {
                return false;
            }
            StringBuilder objectsString = new StringBuilder();
            string objectHeader = sceneStrings[0];
            int countUsesStrings = 1;
            for (int i = 1; i < sceneStrings.Count; i++)
            {

                string objectPropertie = sceneStrings[i];
                if (IsObjectHeader(objectPropertie))
                {
                    break;
                }
                else
                {
                    objectsString.AppendLine(objectPropertie);
                    countUsesStrings++;
                }
            }

            sceneStrings.RemoveRange(0, countUsesStrings);

            SceneObject sceneObject = new SceneObject()
            {
                ObjectHeader = objectHeader,
                ObjectBodyYaml = objectsString.ToString()
            };
            sceneObjects.Add(sceneObject);
            return true;
        }

        private bool IsObjectHeader(string objectPropertie)
        {
            string key = ParserConst.UNITY_SCENE_OBJECT_KEY;
            return objectPropertie.Contains(key);
        }

        private void RemoveServiceInformation(List<string> sceneStrings)
        {
            sceneStrings.RemoveRange(0, 2);
        }
        private void AddHeader(List<string> sceneStrings)
        {
            sceneStrings.Add(ParserConst.UNITY_SCENE_FIRST_STRING);
            sceneStrings.Add(ParserConst.UNITY_SCENE_SECOND_STRING);
        }


    }
}
