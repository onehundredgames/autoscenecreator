﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonSourcesProject
{
    public class ExternalFileObject
    {
        public readonly string FileId;
        public string ProjectFilePath;
        public string ModelFileName;

        public ExternalFileObject(string fileId)
        {
            this.FileId = fileId;
        }
    }
}
