﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using YamlDotNet.Serialization;
using CommonSourcesProject;
using Newtonsoft.Json.Converters;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace CommonSourcesProject
{
    [Serializable()]
    public class SceneObject
    {

        public string ObjectSceneId;
        public string ObjectTypeId;
        public object ObjectBody;

        private string ObjectServiceId;
        private string ObjectType;
         
        private string objectHeader;
        private string objectBodyYaml;

        public string ObjectBodyYaml {
            set {
                objectBodyYaml = value;
                YamlToObjectBody();
            }
        }

        public string ObjectHeader
        {
            set
            {
                objectHeader = value;
                ParseHeader();
            }
        }

        private void ParseHeader()
        {
            string service = objectHeader.Remove(0, ParserConst.UNITY_SCENE_OBJECT_KEY.Length);
            string[] ids = service.Split(ParserConst.UNITY_SCENE_HEADER_SEPARATOR.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            ObjectTypeId = ids[0];
            ObjectSceneId = ids[1];
            ObjectServiceId = new StringBuilder().Append(ParserConst.SERVICE_OBJECT_KEY).Append(ObjectSceneId).ToString();
        }

        private void YamlToObjectBody()
        {
            StringReader r = new StringReader(objectBodyYaml);
            Deserializer deserializer = new Deserializer();
            ObjectBody = deserializer.Deserialize(r);
        }

        public string getObjectHeader()
        {
            return new StringBuilder().
                Append(ParserConst.UNITY_SCENE_OBJECT_KEY).
                Append(ObjectTypeId).
                Append(ParserConst.UNITY_SCENE_HEADER_SEPARATOR).
                Append(ObjectSceneId).
                ToString();
        }
        public string getObjectBodyYaml()
        {
            SerializerBuilder serializerBuilder = new SerializerBuilder();
            serializerBuilder.WithMappingStyleResolver(new UnitySceneMappingStyleResolver());
            Serializer serializer = serializerBuilder.Build() as Serializer;
            string result = serializer.Serialize(ObjectBody);
            return result.Substring(0, result.Length-2);
        }

        internal List<string> GetUsesFilesInObject()
        {
//            MatchCollection matches = Regex.Matches(objectBodyYaml, "fileID: [1-9][0-9]*, ");
            MatchCollection matches = Regex.Matches(objectBodyYaml, "\\{fileID: [1-9][0-9]*, guid: [0-9a-f]{32}, type: 3\\}");
            List<string> fileIds = new List<string>();

            foreach (Match match in matches)
            {
                string value = match.Value;
                Match idMatch = Regex.Match(match.Value, "[0-9a-f]{32}");
                if (idMatch.Success)
                {
                    fileIds.Add(idMatch.Value);
                }
            }
            return fileIds;
        }
    }
}
