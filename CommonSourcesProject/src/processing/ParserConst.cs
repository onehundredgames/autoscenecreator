﻿namespace CommonSourcesProject
{
    public class ParserConst
    {
        public const string UNITY_SCENE_OBJECT_KEY = "--- !u!";
        public const string UNITY_SCENE_HEADER_SEPARATOR = " &";
        public const string SERVICE_OBJECT_KEY = "sceneObject";

        public const string UNITY_SCENE_FIRST_STRING = "%YAML 1.1";
        public const string UNITY_SCENE_SECOND_STRING = "%TAG !u! tag:unity3d.com,2011:";
    }
}
