﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonSourcesProject;

namespace CommonSourcesProject
{
    public class ProcessingModel
    {
        public string sourceUnityFileContent;
        public List<SceneObject> sourceSceneObjectsList = new List<SceneObject>();
        public string sceneModelJsonString;
        public List<ExternalFileObject> externalFileObjects = new List<ExternalFileObject>();
        public string externalFilesModelJsonString;
    }
}
