﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;
using YamlDotNetFM.Serialization;

namespace CommonSourcesProject
{
    class UnitySceneMappingStyleResolver : IMappingStyleResolver
    {
        public MappingStyle Resolve(string key)
        {
            if(key == "m_Script")
            {
                return MappingStyle.Flow;
            }else
            {
                return MappingStyle.Any;
            }
        }

        public MappingStyle Resolve(IObjectDescriptor value)
        {
            dynamic valueObject = value.Value as System.Dynamic.ExpandoObject;
            if(valueObject != null)
            {
                foreach (var property in (IDictionary<String, Object>)valueObject)
                {
                    if (property.Key == "fileID")
                    {
                        return MappingStyle.Flow;
                    }
                }
            }
            return MappingStyle.Any;
        }
    }
}
