﻿using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
using System;

namespace CommonSourcesProject
{
    public class OperatingController
    {
        public bool CheckPath(string path)
        {
            return Directory.Exists(path);
        }
        public bool CheckSceneFileExist(string path)
        {
            return File.Exists(path);
        }

        public List<string> LoadFileToStrings(string path)
        {
            File.OpenRead(path);
            string[] lines = File.ReadAllLines(path);

            List<string> fileStrings = new List<string>();
            for (int i = 0; i < lines.Length; i++)
            {
                fileStrings.Add(lines[i]);
            }

            return fileStrings;

        }

        public string LoadFileToString(string path)
        {
            File.OpenRead(path);
            return File.ReadAllText(path);

        }


        public void SaveToFile(string path, string data)
        {
            File.WriteAllText(path, data);
        }

        public string GetDirectory()
        {
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
            DialogResult result = openFileDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                return openFileDialog.SelectedPath;
            }
            return "";
        }

        public string GetSceneFileForRead()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Unity files (*.unity)|*.unity";
            DialogResult result = openFileDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                return openFileDialog.FileName;
            }
            return "";
        }

        public string GetSceneFileForSave()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Unity files (*.unity)|*.unity";
            DialogResult result = saveFileDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                return saveFileDialog.FileName;
            }
            return "";
        }

        public List<string> FindFilesOnUnityProject(string path, string searchPattern)
        {
            List<string> filesList = new List<string>();
            try
            {
                IEnumerable<string> sceneFiles = Directory.EnumerateFiles(path, searchPattern, SearchOption.AllDirectories);
                foreach (string currentFile in sceneFiles)
                {
                    filesList.Add(currentFile);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return filesList;
        }
    }
}

