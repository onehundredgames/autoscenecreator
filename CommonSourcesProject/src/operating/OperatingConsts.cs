﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonSourcesProject
{
    public class OperatingConsts
    {
        public const string SCENE_MODEL_FILE = "\\scene.json";
        public const string MODEL_EXRERNAL_FILES_FILE = "\\external_files.json";
        public const string SCENE_FILE_IN_PROJECT = "\\Assets\\Scenes\\NewTestScene.unity";
        public const string SCENE_MODEL_RESOURCES_PATH = "\\Resources";
        //        public const string SCENE_FILE_IN_PROJECT = "\\NewTestScene.unity";
    }
}
