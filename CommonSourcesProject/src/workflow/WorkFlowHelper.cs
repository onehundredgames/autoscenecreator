﻿using CommonSourcesProject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace CommonSourcesProject
{
    public class WorkFlowHelper
    {
        private OperatingController operatingController = new OperatingController();
        private ProcessingModel processingModel = new ProcessingModel();
        private ProcessingController processingController = new ProcessingController();

        public void ReadUnityFile(string sourceUnityFilePath)
        {
            processingModel.sourceUnityFileContent = operatingController.LoadFileToString(sourceUnityFilePath);
        }

        public void ReadJsonFile(string sourceJsonFilePath)
        {
            processingModel.sceneModelJsonString = operatingController.LoadFileToString(sourceJsonFilePath);
        }

        public void ConvertYamlStringToObject()
        {
            processingModel.sourceSceneObjectsList = processingController.ConvertUnitySceneStringToObjects(processingModel.sourceUnityFileContent);
        }

        public void CovertSceneObjectToJsonString()
        {
            processingModel.sceneModelJsonString = processingController.GetJsonFromObjects(processingModel.sourceSceneObjectsList);
        }

        public void CovertSceneModelJsonStringToSceneObject()
        {
            processingModel.sourceSceneObjectsList = processingController.GetSceneObjectsFromJson(processingModel.sceneModelJsonString);
        }

        public void ConvertSceneObjectsToYamlString()
        {
            processingModel.sourceUnityFileContent = processingController.ConvertUnitySceneObjectsToString(processingModel.sourceSceneObjectsList);
        }

        public void SaveSceneUnityFile(string destinationUnityFilePath)
        {
            operatingController.SaveToFile(destinationUnityFilePath, processingModel.sourceUnityFileContent);
        }

        public void SaveSceneJsonFile(string destinationJsonFilePath)
        {
            operatingController.SaveToFile(destinationJsonFilePath, processingModel.sceneModelJsonString);
        }

        public void CreateExternalFilesList()
        {
            processingModel.externalFileObjects = processingController.FindExternalFilesInSceneObjects(processingModel.sourceSceneObjectsList);

       }

        public void SetFilePathToExternalFileObjects(string path)
        {
            foreach (ExternalFileObject externalFileObject in processingModel.externalFileObjects)
            {
                SetFilePathToExternalFileObject(externalFileObject, path);
            }
        }

        private void SetFilePathToExternalFileObject(ExternalFileObject externalFileObject, string path)
        {
            List<string> files = operatingController.FindFilesOnUnityProject(path, externalFileObject.FileId+".info");
            if(files.Count == 1)
            {
                string fileContent = operatingController.LoadFileToString(files[0]);
                MatchCollection matches = Regex.Matches(fileContent, "path: .+");
                if(matches.Count == 1)
                {
                    string filePath = Regex.Replace(matches[0].Value, "path: ","");
                    externalFileObject.ProjectFilePath = filePath;
                }
                else
                {
                    throw new System.Exception("WorkFlowHelper.SetFilePathToExternalFileObject: Problem with founded path");
                }
            }
            else
            {
                throw new System.Exception("WorkFlowHelper.SetFilePathToExternalFileObject: Problem with founded files");
            }
        }

        public void CopyResourceFilesToModel(string projectPath, string modelPath)
        {
            foreach (ExternalFileObject externalFileObject in processingModel.externalFileObjects)
            {
                externalFileObject.ModelFileName = externalFileObject.ProjectFilePath.Replace('/','_');
                if (!Directory.Exists(modelPath))
                {
                    Directory.CreateDirectory(modelPath);
                }
                File.Copy(projectPath + '/' + externalFileObject.ProjectFilePath, modelPath + '/' + externalFileObject.ModelFileName,true);
            }
        }

        public void CovertExternalFileObjectsToJsonString()
        {
            processingModel.externalFilesModelJsonString = processingController.GetJsonFromObjects(processingModel.externalFileObjects);
        }

        public void SaveExternalFilesJsonFile(string destinationJsonFilePath)
        {
            operatingController.SaveToFile(destinationJsonFilePath, processingModel.externalFilesModelJsonString);
        }


    }
}
