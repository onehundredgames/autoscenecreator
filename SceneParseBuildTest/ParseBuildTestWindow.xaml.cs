﻿using System.Windows;
using System.Windows.Controls;
using CommonSourcesProject;

namespace SceneParseBuildTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ParseBuildTestWindow : Window
    {
        private ViewModel viewModel = new ViewModel();
        private OperatingController operatingController = new OperatingController();
        private OperatingModel operatingModel = new OperatingModel();

        private WorkFlowHelper workFlowHelper = new WorkFlowHelper();

        public ParseBuildTestWindow()
        {
            InitializeComponent();
        }

        private void SelectFileBtn_Click(object sender, RoutedEventArgs e)
        {
            SelectFileTxt.Text = operatingController.GetSceneFileForRead();
        }

        private void SelectFileTxt_TextChanged(object sender, TextChangedEventArgs e)
        {
            operatingModel.SourceUnityFilePath = SelectFileTxt.Text;
            viewModel.SourceUnityFilePathIsValid = operatingController.CheckSceneFileExist(operatingModel.SourceUnityFilePath);
        }

        private void UseYamlCB_Checked(object sender, RoutedEventArgs e)
        {
            viewModel.UseYaml = true;
            UseJsonCB.IsEnabled = true;
        }
        private void UseYamlCB_Unchecked(object sender, RoutedEventArgs e)
        {
            viewModel.UseYaml = false;
            UseJsonCB.IsEnabled = false;
        }

        private void UseJsonCB_Checked(object sender, RoutedEventArgs e)
        {
            viewModel.UseJson = true;
        }

        private void UseJsonCB_Unchecked(object sender, RoutedEventArgs e)
        {
            viewModel.UseJson = true;
        }

        private void CopyBtn_Click(object sender, RoutedEventArgs e)
        {
            operatingModel.DestinationUnityFilePath = operatingController.GetSceneFileForSave();
            if (viewModel.SourceUnityFilePathIsValid && operatingModel.DestinationUnityFilePath!="")
            {
                workFlowHelper.ReadUnityFile(operatingModel.SourceUnityFilePath);
                if (viewModel.UseYaml)
                {
                    workFlowHelper.ConvertYamlStringToObject();
                    if (viewModel.UseJson)
                    {
                        workFlowHelper.CovertSceneObjectToJsonString();
                        workFlowHelper.CovertSceneModelJsonStringToSceneObject();
                    }
                    workFlowHelper.ConvertSceneObjectsToYamlString();
                }
                workFlowHelper.SaveSceneUnityFile(operatingModel.DestinationUnityFilePath);
            }
        }

    }
}
