﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

namespace YamlDotNetFM.Serialization
{
    public interface IMappingStyleResolver
    {
        MappingStyle Resolve(string key);
        MappingStyle Resolve(IObjectDescriptor value);
    }
}
