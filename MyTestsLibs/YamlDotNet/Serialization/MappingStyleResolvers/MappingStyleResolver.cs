﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

namespace YamlDotNetFM.Serialization.MappingStyleResolvers
{
    class MappingStyleResolver : IMappingStyleResolver
    {
        public MappingStyle Resolve(string key)
        {
            return MappingStyle.Any;
        }

        public MappingStyle Resolve(IObjectDescriptor value)
        {
            return MappingStyle.Any;
        }
    }
}
