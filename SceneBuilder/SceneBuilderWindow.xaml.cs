﻿using System.Windows;
using System.Windows.Controls;
using CommonSourcesProject;

namespace SceneBuilder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SceneBuilderMainWindow : Window
    {

        private OperatingController operatingController = new OperatingController();
        private OperatingModel operatingModel = new OperatingModel();
        private ViewModel viewModel = new ViewModel();
        private WorkFlowHelper workFlowHelper = new WorkFlowHelper();

        public SceneBuilderMainWindow()
        {
            InitializeComponent();
        }

        private void SceneModelFolderPathTxt_TextChanged(object sender, TextChangedEventArgs e)
        {
            viewModel.SceneModelFolderIsValid = operatingController.CheckPath(SceneModelFolderPathTxt.Text);
            if (viewModel.SceneModelFolderIsValid)
            {
                operatingModel.SceneModelFolder = SceneModelFolderPathTxt.Text;
            }
        }

        private void SceneModelFolderPathSelectBtn_Click(object sender, RoutedEventArgs e)
        {
            SceneModelFolderPathTxt.Text = operatingController.GetDirectory();
        }

        private void DestinationProjectFolderPathTxt_TextChanged(object sender, TextChangedEventArgs e)
        {
            viewModel.DestinationProjectFolderIsValid = operatingController.CheckPath(DestinationProjectFolderPathTxt.Text);
            if (viewModel.DestinationProjectFolderIsValid)
            {
                operatingModel.DestinationProjectFolder = DestinationProjectFolderPathTxt.Text;
            }
        }

        private void DestinationProjectFolderPathSelectBtn_Click(object sender, RoutedEventArgs e)
        {
            DestinationProjectFolderPathTxt.Text = operatingController.GetDirectory();
        }

        private void GenerateBtn_Click(object sender, RoutedEventArgs e)
        {
            if(viewModel.DestinationProjectFolderIsValid && viewModel.SceneModelFolderIsValid)
            {
                workFlowHelper.ReadJsonFile(operatingModel.SceneModelFolder + OperatingConsts.SCENE_MODEL_FILE);
                workFlowHelper.CovertSceneModelJsonStringToSceneObject();
                workFlowHelper.ConvertSceneObjectsToYamlString();
                workFlowHelper.SaveSceneUnityFile(operatingModel.DestinationProjectFolder + OperatingConsts.SCENE_FILE_IN_PROJECT);
            }
        }
    }
}
