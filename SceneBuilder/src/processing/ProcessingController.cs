﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonSourcesProject;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace SceneBuilder
{
    class ProcessingController
    {
        public string CreateUnitySceneFromModel(string sceneModelJson)
        {
            List<string> sceneStrings = new List<string>();

            AddHeader(sceneStrings);
            AddObjects(sceneModelJson, sceneStrings);
            StringBuilder stringBuilder = new StringBuilder();
            for ( int i = 0; i< sceneStrings.Count; i++)
            {
                stringBuilder.AppendLine(sceneStrings[i]);
            }
            return stringBuilder.ToString();
        }

        private void AddObjects(string sceneModelJson,List<string> sceneStrings)
        {
            JArray  sceneModel = JsonConvert.DeserializeObject(sceneModelJson) as JArray;
            List<SceneObject> sceneObjects = new List<SceneObject>();

            for( int i = 0; i< sceneModel.Count; i++)
            {
                JObject sceneModelObject = sceneModel[i] as JObject;
                SceneObject sceneObject = new SceneObject()
                {
                    ObjectSceneId = sceneModelObject.GetValue("ObjectSceneId").ToString(),
                    ObjectTypeId = sceneModelObject.GetValue("ObjectTypeId").ToString()
                    
                };

                var expConverter = new ExpandoObjectConverter();
                sceneObject.ObjectBody = JsonConvert.DeserializeObject<ExpandoObject>(sceneModelObject.GetValue("ObjectBody").ToString(), expConverter);

                sceneObjects.Add(sceneObject);
                sceneStrings.Add(sceneObject.getObjectHeader());
                sceneStrings.Add(sceneObject.getObjectBodyYaml());
            }
            Console.WriteLine(sceneModel.GetType());

            
        }

        private void AddHeader(List<string> sceneStrings)
        {
            sceneStrings.Add(ParserConst.UNITY_SCENE_FIRST_STRING);
            sceneStrings.Add(ParserConst.UNITY_SCENE_SECOND_STRING);
        }
    }
}
